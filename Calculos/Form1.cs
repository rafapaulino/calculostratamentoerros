﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculos
{
    public partial class Form1 : Form
    {
        double n1, n2, resultado;

        public Form1()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            converter();
            resultado = n1 + n2;
            MessageBox.Show("O resultado da Soma de " + n1 + " + " + n2 + " = " + resultado, "Resultado da Operação:");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            converter();
            resultado = n1 - n2;
            MessageBox.Show("O resultado da Subtração de " + n1 + " - " + n2 + " = " + resultado, "Resultado da Operação:");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            converter();
            resultado = n1 * n2;
            MessageBox.Show("O resultado da Multiplicação de " + n1 + " X " + n2 + " = " + resultado, "Resultado da Operação:");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            converter();
            resultado = n1 / n2;
            MessageBox.Show("O resultado da Divisão de " + n1 + " / " + n2 + " = " + resultado, "Resultado da Operação:");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            converter();
            resultado = n1 % n2;
            MessageBox.Show("O resultado do Resto da Divisão de " + n1 + " % " + n2 + " = " + resultado, "Resultado da Operação:");
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void converter()
        {
            try
            {
                n1 = double.Parse(textBox1.Text);
                n2 = double.Parse(textBox2.Text);
            }
            catch(Exception error)
            {
                MessageBox.Show("Ocorreu um erro: " + error);
            }
        }
    }
}
